mod product_scrapers;

enum parts {
    title,
    body,
    color,
    brand,
}

trait ProductScraper {
    fn getProductPart(element: &scraper::node::Element, part: parts) -> String;
    fn getProductPrice(element: &scraper::node::Element) -> f32;
    fn getImageUrls(element: &scraper::node::Element) -> i32;
}