mod catalog_scrapers;

pub trait CatalogPage<'a> {
    fn get_links(url: &str) -> Vec<&'a str>;
    fn get_source(&self) -> CatalogSource;
}