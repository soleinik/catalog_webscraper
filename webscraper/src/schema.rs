table! {
    images (id) {
        id -> Int4,
        product_id -> Nullable<Int4>,
        url -> Nullable<Varchar>,
    }
}

table! {
    products (id) {
        id -> Int4,
        title -> Varchar,
        body -> Nullable<Text>,
        price -> Numeric,
        color -> Nullable<Varchar>,
        brand -> Nullable<Varchar>,
        posted -> Bool,
    }
}

joinable!(images -> products (product_id));

allow_tables_to_appear_in_same_query!(
    images,
    products,
);
