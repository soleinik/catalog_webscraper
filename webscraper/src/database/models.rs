
#[derive(Identifiable, Queryable)]
#[table_name = "products"]
pub struct Products {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub price: f64,
    pub color: String,
    pub brand: String,  
    pub published: bool,
}

#[derive(Identifiable, Queryable, Associations)]
#[belongs_to(Products)]
#[table_name = "images"]
pub struct Image {
    pub id: i32,
    pub product_id: i32,
    pub url: String,
}