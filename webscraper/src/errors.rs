use std::error;
use std::num::ParseIntError;
use std::fmt;

type Result<T> = std::result::Result<T, ScraperError>;

#[derive(Debug)]
pub enum ScraperError {
    //PARSE = my error name, wrapping around a REAL error
    Parse(ParseIntError),
}

impl fmt::Display for ScraperError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ScraperError::Parse(ref e) => e.fmt(f),
        }
    }
}

impl error::Error for ScraperError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match *self {
            ScraperError::Parse(ref e) => Some(e),
        }
    }
}

// Implement the conversion from `ParseIntError` to `ScraperError`.
// This will be automatically called by `?` if a `ParseIntError`
// needs to be converted into a `DoubleError`.
impl From<ParseIntError> for ScraperError {
    fn from(err: ParseIntError) -> ScraperError {
        ScraperError::Parse(err)
    }
}

fn print(result: Result<i32>) {
    match result {
        Ok(n)  => println!("{}", n),
        Err(e) => println!("Error: {}", e),
    }
}
