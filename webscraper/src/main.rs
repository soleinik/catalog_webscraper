mod parse;
mod errors;

use parse::begin_scraping;
pub mod scrapers;

/*
example links: 
https://www.pacsun.com/4si3nna/, 
https://www.asos.com/us/women/tops/cat/?cid=4169&nlid=ww%7Cclothing%7Cshop%20by%20product&page=1
*/


#[tokio::main]
async fn main() {
    let urls: Vec<String> = vec![String::from("https://www.pacsun.com/4si3nna/"),
     String::from("https://www.asos.com/us/women/tops/cat/?cid=4169&nlid=ww%7Cclothing%7Cshop%20by%20product&page=1")];
    match begin_scraping(urls){
        Ok(_) => println!("Scrape completed"),
        Err(e) => panic!(e),
    };
}