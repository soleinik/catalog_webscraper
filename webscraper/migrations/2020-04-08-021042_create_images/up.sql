-- Your SQL goes here
CREATE TABLE images (
    id SERIAL PRIMARY KEY,
    product_id INTEGER,
    url VARCHAR,
    FOREIGN KEY (product_id) REFERENCES products (id)
)