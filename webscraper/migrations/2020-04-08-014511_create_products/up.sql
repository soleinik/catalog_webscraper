-- Your SQL goes here
CREATE TABLE products (
  id SERIAL PRIMARY KEY,
  title VARCHAR NOT NULL,
  body TEXT,
  price NUMERIC (5, 2) NOT NULL,
  color VARCHAR,
  brand VARCHAR,
  posted BOOLEAN NOT NULL DEFAULT 'f'
)